const numberFormat = new Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
});

export default function MoneyFormat({ value }) {
  return (
    <span style={{ fontSize: "0.85em" }}>
      {numberFormat.format(value / 100)}
    </span>
  );
}
