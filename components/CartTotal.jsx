import { useContext } from "react";
import { CartContext } from "../context/cart";
import MoneyFormat from "./MoneyFormat";
import styles from "./cartRow.module.scss";
import { getTotalSum } from "../data/api";

const CartTotal = ({ products }) => {
  const { state: items } = useContext(CartContext);
  const totalSum = getTotalSum(products, items);
  return (
    <div className={styles.row}>
      <div className={styles.columnStretched} />
      <div className={styles.column}>
        <strong>
          <MoneyFormat value={totalSum} />
        </strong>
      </div>
    </div>
  );
};

export default CartTotal;
